import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class login extends JFrame implements ActionListener
{
	private JPanel panel;
	private JTextField nama;
	private JPasswordField pas;
	private JLabel nama1,pas1,tit;
	private JButton login,reset,cancel;
	private ImageIcon icon,icon1;
	
	public login()
	{
		super("Welcome to Elite Car Rent");
		setSize(400,400);
		setLocationRelativeTo(null);
		
		panel = new JPanel();
		panel.setLayout(null);
	    panel.setBackground(Color.lightGray);
	
		tit = new JLabel("LOGIN");
		tit.setFont(new Font("Plumeria Sans", Font.BOLD, 50));
		tit.setSize(150, 30);
		tit.setLocation(130,25);
		panel.add(tit);
		
		icon = new ImageIcon("src/image/user1.png");
		
		nama1 = new JLabel(icon);
		nama1.setSize(100,100);
		nama1.setLocation(63,60);
		
		nama = new JTextField();
		nama.setSize(130,35);
		nama.setLocation(180,100);
		nama.addActionListener(this);
		
		icon1 = new ImageIcon("src/image/pass1.png");
		
		pas1 = new JLabel(icon1);
		pas1.setSize(100,100);
		pas1.setLocation(75,150);
		
		pas = new JPasswordField();
		pas.setSize(130,35);
		pas.setLocation(180,180);
		pas.addActionListener(this);
		
		login = new JButton("Login");
		login.setFont(new Font("Rocky Mountain Beauty", Font.PLAIN, 15));
		login.setForeground(Color.white);
		login.setBackground(Color.gray);
		login.setSize(100, 35);
		login.setLocation(20,255);
		login.addActionListener(this);
		
		reset = new JButton("Reset");
		reset.setFont(new Font("Rocky Mountain Beauty", Font.PLAIN, 15));
		reset.setForeground(Color.white);
		reset.setBackground(Color.gray);
		reset.setSize(100,35);
		reset.setLocation(145,255);
		reset.addActionListener(this);
		
		cancel = new JButton("Cancel");
		cancel.setFont(new Font("Rocky Mountain Beauty", Font.PLAIN, 15));
		cancel.setForeground(Color.white);
		cancel.setBackground(Color.gray);
		cancel.setSize(100,35);
		cancel.setLocation(265,255);
		cancel.addActionListener(this);
		
		panel.add(nama1);
		panel.add(nama);
		panel.add(pas1);
		panel.add(pas);
		panel.add(login);
		panel.add(reset);
		panel.add(cancel);
		add(panel);
	}
	
	private static Connection getConnection() throws Exception
	{
		Class.forName("com.mysql.jdbc.Driver");
		String url = "jdbc:mysql://localhost/mobil";
		String uname = "root";
		String passw = "";
		
		return DriverManager.getConnection(url, uname, passw);
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0)
	{
		// TODO Auto-generated method stub
		String s = arg0.getActionCommand();
		Connection con;
	
		if(s.equalsIgnoreCase("Login"))
		{
			try{
				con = getConnection();
				Statement stmt = con.createStatement();
				String sql = "select * from login where user='"+nama.getText()+"' && pass='"+pas.getText()+ "'";
				ResultSet r = stmt.executeQuery(sql);
				
				if(r.next())
				{
					if(nama.getText().equals(r.getString("user")) && pas.getText().equals(r.getString("pass")))
					{
						JOptionPane.showMessageDialog(null,"Login berhasil");
						setVisible(false);
						pilih a = new pilih();
						a.setVisible(true);
					}
				}
				else
				{
					JOptionPane.showMessageDialog(null, "Username dan password tidak tersedia");
				}
				stmt.close();
				con.close();
				setDefaultCloseOperation(EXIT_ON_CLOSE);
			}catch(Exception e )
			{
				JOptionPane.showMessageDialog(null,"Terjadi kesalahan");
			}
		}
		else if(s.equalsIgnoreCase("Reset"))
		{
				nama.setText("");
				pas.setText("");	
		}
		else if(s.equalsIgnoreCase("Cancel"))
		{
			setVisible(false);
			index a = new index();
			a.setVisible(true);	
		}
	}
}