import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class index extends JFrame implements ActionListener
{	 
	 private JButton go,si;
	 private JPanel panel;
	 
	 public index()
	 {
		 super("Elite Car Rent");
		 setSize(573,400);
		 setLocationRelativeTo(null);
		 
		 panel=new ProjekkuPanel();
		 panel.setSize(573,400);
		 panel.setOpaque(false);
		 panel.setVisible(true);
		 
		 go = new JButton("LOGIN");
		 go.setSize(120,35);
		 go.setLocation(165,310);
		 go.setFont(new Font("Rocky Mountain Beauty", Font.PLAIN, 15));
		 go.setForeground(Color.black);
		 go.setBackground(Color.lightGray);
		 go.addActionListener(this);
		 add(go);
		 
		 si = new JButton("SIGN UP");
		 si.setSize(120,35);
		 si.setLocation(285,310);
		 si.setFont(new Font("Rocky Mountain Beauty", Font.PLAIN, 15));
		 si.setForeground(Color.black);
		 si.setBackground(Color.lightGray);
		 si.addActionListener(this);
		 add(si);
		 
		 add(panel);
		 
		 setDefaultCloseOperation(EXIT_ON_CLOSE);
	 }
	 
	 public static void main(String[] args)
	 {
		 index c =new index();
		 c.setVisible(true);
	 }
	 
	 @Override
	 public void actionPerformed(ActionEvent e) 
	 {
		// TODO Auto-generated method stub
		
		String temp = e.getActionCommand();
		
		if(temp.equalsIgnoreCase("LOGIN"))
		{
			setVisible(false);
			login a = new login();
			a.setVisible(true);
		}
		else if(temp.equalsIgnoreCase("SIGN UP"))
		{
			setVisible(false);
			signup a = new signup();
			a.setVisible(true);
		}
	}
 }