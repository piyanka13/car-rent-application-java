import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Ertiga extends JFrame implements ActionListener
{
	private JLabel lama,nama,tanggal,tit,harga1,harga,tit1,namap,namap1,tgl,tgl1,lamap,lamap1,hrg,hrg1,total,total2;
	private JTextField lama1, nama1,tanggal1,total1;
	private JButton sele,cancel,ok;
	private JPanel panel, panel1;
	
	public Ertiga()
	{
		super("Elite Car Rent");
		setSize(360,500);
		setLocationRelativeTo(null);
		
		panel = new JPanel();
		panel.setLayout(null);
		panel.setSize(360,500);
		panel.setLocation(0,0);
		panel.setBackground(Color.lightGray);
		
		tit = new JLabel("Rent");
		tit.setFont(new Font("Plumeria Sans", Font.BOLD, 50));
		tit.setSize(200,40);
		tit.setLocation(130,10);
		panel.add(tit);
	
		nama = new JLabel("Nama Pemesan :");
		nama.setSize(150,40);
		nama.setLocation(20,73);
		nama.setFont(new Font("Rocky Mountain Beauty", Font.PLAIN, 16));

		nama1 = new JTextField();
		nama1.setSize(140,25);
		nama1.setLocation(180,80);
		nama1.addActionListener(this);
		
		tanggal = new JLabel("Tanggal pinjam :");
		tanggal.setSize(150,40);
		tanggal.setLocation(20,113);
		tanggal.setFont(new Font("Rocky Mountain Beauty", Font.PLAIN, 16));
		
		tanggal1= new JTextField();
		tanggal1.setSize(140,25);
		tanggal1.setLocation(180,120);
		tanggal1.addActionListener(this);
	
		lama = new JLabel("Lama pinjam (hari) :");
		lama.setSize(150,40);
		lama.setLocation(20,150);
		lama.setFont(new Font("Rocky Mountain Beauty", Font.PLAIN, 16));
		
		lama1 = new JTextField();
		lama1.setSize(140,25);
		lama1.setLocation(180,159);
		lama1.addActionListener(this);
		
		harga = new JLabel("Harga/hari :");
		harga.setSize(150,40);
		harga.setLocation(20,186);
		harga.setFont(new Font("Rocky Mountain Beauty", Font.PLAIN, 16));
		
		harga1 = new JLabel();
		harga1.setText("Rp. 250000");
		harga1.setFont(new Font("Rocky Mountain Beauty", Font.PLAIN, 16));
		harga1.setSize(140,25);
		harga1.setLocation(180,194);

		total1 = new JTextField();
		total1.setSize(140,25);
		total1.setLocation(100,300);
		total1.setVisible(false);
		total1.addActionListener(this);

		sele = new JButton("Preview");
		sele.setFont(new Font("Rocky Mountain Beauty", Font.PLAIN, 14));
		sele.setForeground(Color.white);
		sele.setBackground(Color.gray);
		sele.setSize(120,35);
		sele.setLocation(40,400);
		sele.addActionListener(this);
		
		cancel = new JButton("Cancel");
		cancel.setFont(new Font("Rocky Mountain Beauty", Font.PLAIN, 14));
		cancel.setForeground(Color.white);
		cancel.setBackground(Color.gray);
		cancel.setSize(120,35);
		cancel.setLocation(180,400);
		cancel.addActionListener(this);
	
		panel.add(nama);
	    panel.add(nama1);
		panel.add(tanggal);
		panel.add(tanggal1);
		panel.add(lama);
		panel.add(lama1);
		panel.add(harga);
		panel.add(harga1);
		panel.add(total1);
		panel.add(sele);
		panel.add(cancel);
		add(panel);
	}
	
	public void totalw()
	{	
		 int tot = 0;
		 int s = 250000;
		 String lama= lama1.getText();
		 int la = Integer.parseInt(lama);
	
		 tot = s*la;
		 total1.setText(Integer.toString(tot));
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) 
	{
		// TODO Auto-generated method stub
		
		String ass= arg0.getActionCommand();
		
		if(ass.equalsIgnoreCase("Total"))
		{
				totalw();	
		}
		else if(ass.equalsIgnoreCase("Preview"))
		{
			nama1.getText();
			tanggal1.getText();
			lama1.getText();
			harga1.getText();
			total1.getText();
	
			panel.setVisible(false);
			
			totalw();
			
			panel1 = new JPanel();
			panel1.setLayout(null);
			panel1.setBackground(Color.lightGray);
			panel1.setVisible(false);
			add(panel1);
			
			tit1 = new JLabel("Detail Penyewaan");
			tit1.setFont(new Font("Plumeria Sans", Font.BOLD, 40));
			tit1.setSize(300,40);
			tit1.setLocation(50,30);
			panel1.add(tit1);
					
			namap = new JLabel("Nama Pemesan :");
			namap.setSize(150,40);
			namap.setLocation(25,100);
			namap.setFont(new Font("Rocky Mountain Beauty", Font.PLAIN, 16));
			panel1.add(namap);
					
			namap1 = new JLabel(nama1.getText());
			namap1.setSize(150,40);
			namap1.setLocation(200,100);
			namap1.setFont(new Font("Rocky Mountain Beauty", Font.PLAIN, 16));
			panel1.add(namap1);
			
			tgl = new JLabel("Tanggal pesan :");
			tgl.setSize(150,40);
			tgl.setLocation(25,150);
			tgl.setFont(new Font("Rocky Mountain Beauty", Font.PLAIN, 16));
			panel1.add(tgl);
			
			tgl1 = new JLabel(tanggal1.getText());
			tgl1.setSize(150,40);
			tgl1.setLocation(200,150);
			tgl1.setFont(new Font("Rocky Mountain Beauty", Font.PLAIN, 16));
			panel1.add(tgl1);
			
			lamap = new JLabel("Lama peminjaman :");
			lamap.setSize(150,40);
			lamap.setLocation(25,200);
			lamap.setFont(new Font("Rocky Mountain Beauty", Font.PLAIN, 16));
			panel1.add(lamap);
			
			lamap1 = new JLabel(lama1.getText() +" hari");
			lamap1.setSize(150,40);
			lamap1.setLocation(200,200);
			lamap1.setFont(new Font("Rocky Mountain Beauty", Font.PLAIN, 16));
			panel1.add(lamap1);
			
			hrg = new JLabel("Harga/hari :");
			hrg.setSize(110,40);
			hrg.setLocation(25,250);
			hrg.setFont(new Font("Rocky Mountain Beauty", Font.PLAIN, 16));
			panel1.add(hrg);
			
			hrg1 = new JLabel(harga1.getText());
			hrg1.setSize(150,40);
			hrg1.setLocation(200,250);
			hrg1.setFont(new Font("Rocky Mountain Beauty", Font.PLAIN, 16));
			panel1.add(hrg1);
			
			total = new JLabel("Total pembayaran:");
			total.setSize(150,40);
			total.setLocation(25,300);
			total.setFont(new Font("Rocky Mountain Beauty", Font.PLAIN, 16));
			panel1.add(total);
			
			total2 = new JLabel("Rp. "+total1.getText());
			total2.setSize(150,40);
			total2.setLocation(200,300);
			total2.setFont(new Font("Rocky Mountain Beauty", Font.PLAIN, 16));
			panel1.add(total2);
						
			ok = new JButton("Ok");
			ok.setFont(new Font("Rocky Mountain Beauty", Font.PLAIN, 14));
			ok.setForeground(Color.white);
			ok.setBackground(Color.gray);
			ok.setSize(100,35);
			ok.setLocation(120,400);
			ok.addActionListener(this);
			panel1.add(ok);
			
			panel1.setVisible(true);
		}
		else if(ass.equalsIgnoreCase("Cancel"))
		{
			setVisible(false);
			pilih a= new pilih();
			a.setVisible(true);
		}
		else if(ass.equalsIgnoreCase("Ok"))
		{
			JOptionPane.showMessageDialog(null, "Terima kasih");
			setVisible(false);
			index c = new index();
			c.setVisible(true);
		}
	}
}