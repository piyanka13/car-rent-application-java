import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.JPanel;

public class signup extends JFrame implements ActionListener
{
	private JTextField nama,ktp,alamat,email,tel,user;
	private JPasswordField pass;
	private JComboBox list ;
	private JPanel panel;
	private JLabel tit,nama1,alamat1,email1,tel1,user1,jenkel1,pass1,ktp1;
	private JButton daf,can,re;
	
	public signup()
	{
		super("Welcome to Elite Car Rent");
		setSize(360,500);
		setLocationRelativeTo(null);
		
		panel = new JPanel();
		panel.setLayout(null);
		panel.setBackground(Color.lightGray);
		add(panel);
		
		tit = new JLabel("Sign Up");
		tit.setFont(new Font("Plumeria Sans", Font.BOLD, 50));
		tit.setSize(200,55);
		tit.setLocation(110,10);
		panel.add(tit);
	
		nama1 = new JLabel("Nama :");
		nama1.setSize(80,40);
		nama1.setLocation(40,70);
		nama1.setFont(new Font("Rocky Mountain Beauty", Font.PLAIN, 15));
		
		nama = new JTextField();
		nama.setSize(140,25);
		nama.setLocation(155,80);
		nama.addActionListener(this);
		
		ktp1 = new JLabel("No.KTP :");
		ktp1.setSize(80,40);
		ktp1.setLocation(40,110);
		ktp1.setFont(new Font("Rocky Mountain Beauty", Font.PLAIN, 15));
		
		ktp = new JTextField();
		ktp.setSize(140,25);
		ktp.setLocation(155,120);
		ktp.addActionListener(this);
		
		alamat1 = new JLabel("Alamat :");
		alamat1.setSize(80,40);
		alamat1.setLocation(40,150);
		alamat1.setFont(new Font("Rocky Mountain Beauty", Font.PLAIN, 15));
		
		alamat = new JTextField();
		alamat.setSize(140,25);
		alamat.setLocation(155,159);
		alamat.addActionListener(this);

		jenkel1 = new JLabel("Jenis Kelamin :");
		jenkel1.setSize(117, 40);
		jenkel1.setLocation(40, 190);
		jenkel1.setFont(new Font("Rocky Mountain Beauty", Font.PLAIN, 15));
		String a[]= {"","Laki-Laki","Perempuan"};
		
		list = new JComboBox(a);
		list.setSize(140,25);
		list.setLocation(155,200);
		list.addActionListener(this);
		
		tel1 = new JLabel("No.Telepon :");
		tel1.setSize(117,40);
		tel1.setLocation(40, 230);
		tel1.setFont(new Font("Rocky Mountain Beauty", Font.PLAIN, 15));
		
		tel = new JTextField();
		tel.setSize(140,25);
		tel.setLocation(155, 240);
		tel.addActionListener(this);
		
		email1 = new JLabel("E-mail :");
		email1.setSize(80, 40);
		email1.setLocation(40, 270);
		email1.setFont(new Font("Rocky Mountain Beauty", Font.PLAIN, 15));
		
		email = new JTextField();
		email.setSize(140,25);
		email.setLocation(155, 280);
		email.addActionListener(this);
		
		user1 = new JLabel("Username :");
		user1.setSize(117, 40);
		user1.setLocation(40,310);
		user1.setFont(new Font("Rocky Mountain Beauty", Font.PLAIN, 15));
		
		user = new JTextField();
		user.setSize(140,25);
		user.setLocation(155, 319);
		user.addActionListener(this);
		
		pass1 = new JLabel("Password :");
		pass1.setSize(100,40);
		pass1.setLocation(40, 350);
		pass1.setFont(new Font("Rocky Mountain Beauty", Font.PLAIN, 15));
		
		pass = new JPasswordField();
		pass.setSize(140,25);
		pass.setLocation(155, 360);
		pass.addActionListener(this);
		
		daf = new JButton("Sign Up");
		daf.setFont(new Font("Rocky Mountain Beauty", Font.PLAIN, 15));
		daf.setForeground(Color.white);
		daf.setBackground(Color.gray);
		daf.setSize(95,35);
		daf.setLocation(25,410);
		daf.addActionListener(this);
		
		can = new JButton("Cancel");
		can.setFont(new Font("Rocky Mountain Beauty", Font.PLAIN, 15));
		can.setForeground(Color.white);
		can.setBackground(Color.gray);
		can.setSize(95,35);
		can.setLocation(126,410);
		can.addActionListener(this);
		
		re = new JButton("Reset");
		re.setFont(new Font("Rocky Mountain Beauty", Font.PLAIN, 15));
		re.setForeground(Color.white);
		re.setBackground(Color.gray);
		re.setSize(95,35);
		re.setLocation(226, 410);
		re.addActionListener(this);

		panel.add(nama1);
	    panel.add(nama);
		panel.add(ktp1);
		panel.add(ktp);
	    panel.add(alamat1);
		panel.add(alamat);
		panel.add(jenkel1);
		panel.add(list);
		panel.add(tel1);
		panel.add(tel);
		panel.add(email1);
		panel.add(email);
		panel.add(user1);
		panel.add(user);
		panel.add(pass1);
		panel.add(pass);
		panel.add(daf);
		panel.add(can);
		panel.add(re);
		add(panel);
	}
	
	private static Connection getConnection () throws Exception
	{
		Class.forName("com.mysql.jdbc.Driver");
		String url = "jdbc:mysql://localhost/mobil";
		String uname = "root";
		String pasw = "";
		return DriverManager.getConnection(url,uname,pasw);
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) 
	{
		// TODO Auto-generated method stub
		String s = arg0.getActionCommand();
		Connection con;
		
		if(s.equalsIgnoreCase("Sign Up"))
		{
			try{
				con = getConnection();
				PreparedStatement st= con.prepareStatement( "INSERT INTO  mobil.daftar values (default, ?, ?, ?, ?, ?, ?)");
				PreparedStatement ss= con.prepareStatement("INSERT INTO mobil.login values (default, ?, ?)" );
		
				st.setString(1,nama.getText());
				st.setString(2,ktp.getText());
				st.setString(3,alamat.getText());
				st.setString(4,list.getSelectedItem().toString());
				st.setString(5,tel.getText());
				st.setString(6,email.getText());
				
				ss.setString(1,user.getText());
				ss.setString(2, pass.getText());
				ss.executeUpdate();
				st.executeUpdate();
			
				ss.close(); 
				st.close();
				con.close();
				JOptionPane.showMessageDialog(null, "Terima kasih telah mendaftar");
				setVisible(false);
				login a = new login();
				a.setVisible(true);
			}catch(Exception e1)
			{
				e1.printStackTrace();
				JOptionPane.showMessageDialog(null, 
						"Maaf cek koneksi anda");
			}
		}
		if(s.equalsIgnoreCase("Cancel"))
		{
			setVisible(false);
			index a = new index();
			a.setVisible(true);
		}
		else if(s.equalsIgnoreCase("Reset"))
		{
				nama.setText("");
				ktp.setText("");
				alamat.setText("");
				list.setSelectedItem("");
				tel.setText("");
				email.setText("");
				user.setText("");
				pass.setText("");
		}
	}
}