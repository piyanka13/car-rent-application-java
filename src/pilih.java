import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class pilih extends JFrame implements ActionListener
{
	private JComboBox list;
	private JPanel panel;
	private JLabel tit,jenkel1;
	private JButton daf,cancel;
	private Connection con;
	
	public pilih()
	{
		super("Elite Car Rent");
		setSize(500,430);
		setLocationRelativeTo(null);
	
		panel = new JPanel();
		panel.setLayout(null);
		panel.setBackground(Color.lightGray);
		add(panel);
	
		tit = new JLabel("Elite Car Showroom");
		tit.setFont(new Font("Plumeria Sans", Font.BOLD, 50));
		tit.setSize(450,40);
		tit.setLocation(70,30);
		panel.add(tit);
	
		jenkel1 = new JLabel("Jenis mobil :");
		jenkel1.setSize(127, 40);
		jenkel1.setLocation(100, 100);
		jenkel1.setFont(new Font("Rocky Mountain Beauty", Font.PLAIN, 16));
	
		list = new JComboBox();
		list.setSize(140,25);
		list.setLocation(225,109);
		rental();
		list.addActionListener(this);
		
		daf = new JButton("Rent");
		daf.setFont(new Font("Rocky Mountain Beauty", Font.PLAIN, 15));
		daf.setForeground(Color.white);
		daf.setBackground(Color.gray);
		daf.setSize(120, 35);
		daf.setLocation(245,158);
		daf.addActionListener(this);
		
		cancel = new JButton("Cancel");
		cancel.setFont(new Font("Rocky Mountain Beauty", Font.PLAIN, 15));
		cancel.setForeground(Color.white);
		cancel.setBackground(Color.gray);
		cancel.setSize(120,35);
		cancel.setLocation(100,158);
		cancel.addActionListener(this);
		
		panel.add(jenkel1);
		panel.add(list);
		panel.add(daf);
		panel.add(cancel);
		add(panel);
	}
	
	private static Connection getConnection () throws Exception
	{
		Class.forName("com.mysql.jdbc.Driver");
		String url = "jdbc:mysql://localhost/mobil";
		String uname = "root";
		String pasw = "";
		return DriverManager.getConnection(url,uname,pasw);
	}
	
	public void rental()
	{
		try{
			con = getConnection();
			Statement stmt =  con.createStatement();
			String sql = "select * from deskripsi";
			ResultSet rs =stmt.executeQuery(sql);
			list.addItem("Pilih");
			
			while(rs.next())
			{
				list.addItem(rs.getString(3));
			}
			stmt.close();
			con.close();
		}catch(Exception e)
		{
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, 
					"Maaf cek koneksi anda");
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) 
	{
		// TODO Auto-generated method stub
		 String a = arg0.getActionCommand();
		 
		 if(a.equalsIgnoreCase("Rent"))
		 {
			 if(list.getSelectedItem().equals("Avanza"))
			 {
				 setVisible(false);
				 avanza av = new avanza();
				 av.setVisible(true);
			 }
			 else if(list.getSelectedItem().equals("Innova"))
			 {
				 setVisible(false);
				 inova in = new inova();
				 in.setVisible(true); 
			 }
			 else if(list.getSelectedItem().equals("Yaris"))
			 {
				 setVisible(false);
				 Yaris yr = new Yaris();
				 yr.setVisible(true);
			 }
			 else if(list.getSelectedItem().equals("CRV"))
			 {
				 setVisible(false);
				 Crv cr = new Crv();
				 cr.setVisible(true);
			 }
			 else if(list.getSelectedItem().equals("Mobilio"))
			 {
				 setVisible(false);
				 Mobilio m = new Mobilio();
				 m.setVisible(true);
			 }
			 else if(list.getSelectedItem().equals("Jazz"))
			 {
				 setVisible(false);
				 Jazz jz = new Jazz();
				 jz.setVisible(true);
			 }
			 else if(list.getSelectedItem().equals("Ertiga"))
			 {
				 setVisible(false);
				 Ertiga er = new Ertiga();
				 er.setVisible(true);
			 }
			 else if(list.getSelectedItem().equals("Grand Vitara"))
			 {
				 setVisible(false);
				 GVitara gv = new GVitara();
				 gv.setVisible(true);
			 }
			 else if(list.getSelectedItem().equals("APV"))
			 {
				 setVisible(false);
				 Apv ap = new Apv();
				 ap.setVisible(true);
			 }
			 else if(list.getSelectedItem().equals("Grand Livina"))
			 {
				 setVisible(false);
				 GLivina gl = new GLivina();
				 gl.setVisible(true);
			 }
		 }
		 else if(a.equalsIgnoreCase("Cancel"))
		 {
			 JOptionPane.showMessageDialog(null, "Terima kasih");
			 setVisible(false);
			 index i= new index();
				i.setVisible(true);
		 }
	}
}